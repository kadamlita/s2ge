#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <list>

SDL_Window* win = NULL;
SDL_Renderer* rend = NULL;

TTF_Font* font = NULL;

SDL_Event input;
bool running = true;

double delta = 0;
Uint64 lastFrame = SDL_GetPerformanceCounter();
Uint64 currentFrame = 0;

struct Transform{
	int x;
	int y;
	int width;
	int height;
};

//TODO
//write a colour class
/* struct Color{ */
/* 	int r; */
/* 	int g; */
/* 	int b; */
/* 	int a; */
/* }; */
//TODO
//write a font system with sizes and colours

SDL_Texture* createGraphic(const char* path){
	SDL_Surface* image = IMG_Load(path);
	if(image == NULL){
		printf("Image %s failed to load.\n", path);
	}
	SDL_Texture* texture = SDL_CreateTextureFromSurface(rend, image);
	SDL_FreeSurface(image);
	return texture;

}

class Entity{
	private:
		bool active;
		SDL_Texture* sprite;
	public:
		Transform transform;
		bool collided;

		bool isActive(){
			return active;
		}

		int Activate(){
			active = true;
			return 0;
		}

		/* SDL_Texture* getSprite(){ */
		/* 	return sprite; */
		/* } */

		int setSprite(SDL_Texture* img){
			sprite = img;
			return 0;
		}

	int Draw(){
		if(active){
			SDL_Rect dst = { transform.x, transform.y, transform.width, transform.height };
			SDL_RenderCopy(rend, sprite, NULL, &dst);
		}
		return 0;
	}

	int Destroy(){
		if(active){
			SDL_DestroyTexture(sprite);
			active = false;
		}
		return 0;
	}
};

std::list<Entity> entities;

int registerEntity(Entity ent){
	entities.push_front(ent);
	return 0;
}

Entity createEntity(const char* name, int nx, int ny, int nwidth, int nheight){
	Entity ent;
	ent.setSprite(createGraphic(name));
	ent.transform.x = nx;
	ent.transform.y = ny;
	ent.transform.width = nwidth;
	ent.transform.height = nheight;
	ent.Activate();
	registerEntity(ent);
	return ent;
}

int Initialise(const char* title = "Game", int width = 720, int height = 480){
	printf("Starting game.\n");

	SDL_Init(SDL_INIT_EVERYTHING);
	IMG_Init(IMG_INIT_PNG);
	TTF_Init();
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);

	win = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, 0);
	rend = SDL_CreateRenderer(win, 0, -1);

	font = TTF_OpenFont("engine/FreeSans.ttf", 25);
	if(font == NULL){
		printf("Font FreeSans failed to load.\n");
	}

	SDL_SetRenderDrawColor(rend, 81, 144, 245, 255);

	return 0;
}

int Clear(){
	SDL_RenderClear(rend);
	return 0;
}

int Render(){
	SDL_RenderPresent(rend);
	return 0;
}

char* intToText(int i){
	char* text;
	sprintf(text, "%d", i);
	return text;
}

SDL_Texture* createText(const char* text){
	SDL_Color tcolor = { 255, 255, 255, 255 };
	SDL_Surface* surface = TTF_RenderText_Solid(font, text, tcolor);
	if(surface == NULL){
		printf("Text %s failed to load.\n", text);
	}
	SDL_Texture* texture = SDL_CreateTextureFromSurface(rend, surface);
	SDL_FreeSurface(surface);
	return texture;
}

Mix_Music* loadMusic(const char* path){
	 Mix_Music* music = Mix_LoadMUS(path);
	 if(music == NULL){
		 printf("Music %s failed to load.\n", path);
	 }
	 return music;
}


class Text{
	private:
		SDL_Texture* sprite;
		bool active;
	public:
		Transform transform;

		Text(){
		}

		bool isActive(){
			return active;
		}

		/* SDL_Texture* getSprite(){ */
		/* 	return sprite; */
		/* } */

	int Draw(){
		if(active){
			int sprW;
			int sprH;
			SDL_QueryTexture(sprite, NULL, NULL, &sprW, &sprH);
			SDL_Rect dst = { transform.x, transform.y, sprW, sprH };
			SDL_RenderCopy(rend, sprite, NULL, &dst);
		}
		return 0;
	}

	int Activate(){
		active = true;
		return 0;
	}

	int Destroy(){
		if(active){
			SDL_DestroyTexture(sprite);
			active = false;
		}
		return 0;
	}

	int updateText(const char* text){
		SDL_DestroyTexture(sprite);
		sprite = createText(text);
		return 0;
	}

	int setSprite(SDL_Texture* text){
		sprite = text;
		return 0;
	}

};

std::list<Text> text_elements;

int registerText(Text text){
	text_elements.push_front(text);
	return 0;
}

Text createTextElement(const char* text, int nx, int ny){
	Text tex;
	tex.setSprite(createText(text));
	tex.transform.x = nx;
	tex.transform.y = ny;
	tex.Activate();
	registerText(tex);
	return tex;
}

class Song{
	private:
		Mix_Music* music;

	public:
		int setMusic(Mix_Music* song){
			music = song;
			return 0;
		}

		Mix_Music* getMusic(){
			return music;
		}

		int Remove(){
			/* Mix_HaltMusic(); */
			Mix_FreeMusic(music);
			return 0;
		}

		int Start(){
			Mix_PlayMusic(music, -1);
			return 0;
		}
};

std::list<Song> songs;

int registerSong(Song song){
	songs.push_front(song);
	return 0;
}

Song loadSong(const char* name){
	Song song;
	song.setMusic(loadMusic(name));
	if(song.getMusic() == NULL){
		printf("Failed to load song %s\n", name);
	}
	registerSong(song);
	return song;
}

int pauseMusic(){
	Mix_PauseMusic();
	return 0;
}

int resumeMusic(){
	Mix_ResumeMusic();
	return 0;
}

int changeMusic(Song music){
	Mix_HaltMusic();
	music.Start();
	return 0;
}

class Sound{
	private:
		Mix_Chunk* sound;
	
	public:
		int Remove(){
			Mix_FreeChunk(sound);
			return 0;
		}

		int Play(){
			Mix_PlayChannel(-1, sound, 0);
			return 0;
		}

		Mix_Chunk* getSound(){
			return sound;
		}

		int setSound(Mix_Chunk* sfx){
			sound = sfx;
			return 0;
		}
};

std::list<Sound> sounds;

int registerSound(Sound sound){
	sounds.push_front(sound);
	return 0;
}

Sound loadSound(const char* name){
	Sound sound;
	sound.setSound(Mix_LoadWAV(name));
	if(sound.getSound() == NULL){
		printf("Failed to load %s sound effect!\n", name);
	}
	registerSound(sound);
	return sound;
}


int Cleanup(){
	printf("Destroying text.\n");

	for(Text t : text_elements){
		text_elements.front().Destroy();
		printf("Text successfully destroyed\n");
	}

	printf("Moving on to entities.\n");

	for(Entity e : entities){
		entities.front().Destroy();
		printf("Entity successfully destroyed.\n");
	}

	printf("Moving on to songs.\n");

	for(Song s : songs){
		songs.front().Remove();
		printf("Song successfully destroyed\n");
	}

	printf("Moving on to sounds.\n");

	for (Sound s : sounds){
		sounds.front().Remove();
		printf("Sound successfully destroyed.\n");
	}

	printf("All elements destroyed.\n");

	TTF_CloseFont(font);
	SDL_DestroyRenderer(rend);
	SDL_DestroyWindow(win);

	Mix_Quit();
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	
	return 0;
}


int Logic(){
	SDL_Delay(30);

	lastFrame = currentFrame;
	currentFrame = SDL_GetPerformanceCounter();
	delta = (double)((currentFrame - lastFrame)*1000 / (double)SDL_GetPerformanceFrequency());

	SDL_PollEvent(&input);

	if(input.type == SDL_QUIT){
		running = false;
	}

	return 0;
}

bool getKeyLeft(){
	if(input.type == SDL_KEYDOWN && input.key.keysym.sym == SDLK_LEFT){
		return true;
	}

	return false;
}

bool getKeyRight(){
	if(input.type == SDL_KEYDOWN && input.key.keysym.sym == SDLK_RIGHT){
		return true;
	}

	return false;
}

bool getKeyUp(){
	if(input.type == SDL_KEYDOWN && input.key.keysym.sym == SDLK_UP){
		return true;
	}

	return false;
}

bool getKeyDown(){
	if(input.type == SDL_KEYDOWN && input.key.keysym.sym == SDLK_DOWN){
		return true;
	}

	return false;
}

bool Collided(Entity entity1, Entity entity2){
	SDL_Rect e1dst = {entity1.transform.x, entity1.transform.y, entity1.transform.width, entity1.transform.height};
	SDL_Rect e2dst = {entity2.transform.x, entity2.transform.y, entity2.transform.width, entity2.transform.height};
	return SDL_HasIntersection(&e1dst, &e2dst);
}

int Log(const char* message){
	printf(message);
	printf("\n");
	return 0;
}
