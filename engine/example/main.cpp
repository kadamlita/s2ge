#include "engine/engine.cpp"

int main(){
	Initialise();

	while(running){
		Logic();

		Clear();
		Render();
	}

	Cleanup();
	return 0;
}
